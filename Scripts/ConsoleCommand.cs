using System;

public class BaseCommand
{
    public string name { get; protected set; }
    public string description { get; protected set; }
    public string format { get; protected set; }

    public BaseCommand(string name, string description, string format)
    {
        this.name = name;
        this.description = description;
        this.format = format;
    }
}

public class ActionCommand : BaseCommand
{
    private Action command;

    public ActionCommand(string name, string description, string format, Action command)
			: base(name, description, format)
    {
        this.command = command;
    }

    public void Invoke() => command.Invoke();
}


public class ActionCommand<T> : BaseCommand
{
    private Action<T> command;

    public ActionCommand(string name, string description, string format, Action<T> command) 
			: base(name, description, format)
    {
        this.command = command;
    }

    public void Invoke(T arg) => command.Invoke(arg);
}
public class ActionCommand<T1, T2> : BaseCommand
{
    private Action<T1, T2> command;

    public ActionCommand(string name, string description, string format, Action<T1, T2> command) 
			: base(name, description, format)
    {
        this.command = command;
    }

    public void Invoke(T1 arg, T2 arg2) => command.Invoke(arg, arg2);
}

public class ActionCommand<T1, T2, T3> : BaseCommand
{
    private Action<T1, T2, T3> command;

    public ActionCommand(string name, string description, string format, Action<T1, T2, T3> command) 
			: base(name, description, format)
    {
        this.command = command;
    }

    public void Invoke(T1 arg, T2 arg2, T3 arg3) => command.Invoke(arg, arg2, arg3);
}

// CREATE NEW COMMAND CLASSES HERE IF NEEDED