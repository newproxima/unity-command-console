using System;
using System.Collections.Generic;
using UnityEngine;

public partial class ConsoleController : MonoBehaviour
{
    public event EventHandler ConsoleCleared;
    public event MessagePrintedHandler MessagePrinted;
    public delegate void MessagePrintedHandler(object sender, ConsoleMessage message);

    public static ConsoleController _instance;

    public List<string> predictions { get; private set; }
    private List<BaseCommand> commands;

    private void Awake()
    {
        // the console being a unique element, this classes makes use of a singleton pattern where
        // _instance is used as a way to reference the instance of this script via a static property
        if(_instance != null) throw new System.Exception("There may only be one instance of ConsoleController");
        else _instance = this;

        predictions = new List<string>();

        // Initialization function (in "ConsoleControllerInit.cs")
        CommandsInit();
    }

    private void PrintToConsole(ConsoleMessage message)
    {
        MessagePrinted?.Invoke(this, message);
    }

    public void ValidateInput(string _input)
    {
        if(_input != "")
        {
            int index = FindCommandIndex(_input);
            if(index >= 0 && index < commands.Count)
            {
                var tokens = _input.Split(' ');
                if(tokens != null) 
                {
                    // if the index is within the commands list bounds
                    // see if it fits the defined patterns below
                    // (to add support for a different command pattern,
                    // add it below)
                    // note: keyword 'is' performs a type check and a type cast aswell
                    if(commands[index] is ActionCommand noArgC)
                    {
                        noArgC.Invoke();
                    }
                    else if(commands[index] is ActionCommand<string> stringArgC)
                    {
                        if(tokens.Length > 1)
                            stringArgC.Invoke(tokens[1]);
                        else
                            PrintToConsole(ConsoleMessage.Error(ConsoleMessage.ErrorTypes.MissingArgument, _input, stringArgC.format, stringArgC.name.ToUpper()));
                    }
                    else if(commands[index] is ActionCommand<int> intArgC)
                    {
                        if(tokens.Length > 1)
                        {
                            if(int.TryParse(tokens[1], out int val))
                                intArgC.Invoke(val);
                            else
                                PrintToConsole(ConsoleMessage.Error(ConsoleMessage.ErrorTypes.InvalidArgument, _input, intArgC.format, intArgC.name.ToUpper()));
                        }
                        else
                        {
                            PrintToConsole(ConsoleMessage.Error(ConsoleMessage.ErrorTypes.MissingArgument, _input, intArgC.format, intArgC.name.ToUpper()));
                        }
                    }
                    else if(commands[index] is ActionCommand<int, int> int2ArgsC)
                    {
                        if(tokens.Length > 2)
                        {
                            if(int.TryParse(tokens[1], out int val1) && int.TryParse(tokens[2], out int val2))
                                int2ArgsC.Invoke(val1, val2);
                            else
                                PrintToConsole(ConsoleMessage.Error(ConsoleMessage.ErrorTypes.InvalidArgument, _input, int2ArgsC.format, int2ArgsC.name.ToUpper()));
                        }
                        else
                        {
                            PrintToConsole(ConsoleMessage.Error(ConsoleMessage.ErrorTypes.MissingArgument, _input, int2ArgsC.format, int2ArgsC.name.ToUpper()));
                        }
                    }
                }
            }
            else
                PrintToConsole(ConsoleMessage.Error(ConsoleMessage.ErrorTypes.InvalidCommand, _input));         
        }

        predictions.Clear();
    }

    private int FindCommandIndex(string _input)
    {
        // if input is empty, return
        if(_input == "") return -1;

        // split the input into separate tokens
        var tokens = _input.Split(' ');
        if(tokens == null) return -1; // if no tokens, return

        for (int i = 0; i < commands.Count; i++) // go through all the available commands
        {
            // if the current command corresponds, return the index
            if(tokens[0] == commands[i].name) 
                return i;
        }
    
        return -1;
    }

    public void PredictCommand(string _input)
    {
        predictions = new List<string>();

        if(_input == "") return;
        
        var inputTokens = _input.Split(' ');
        if(inputTokens == null) return;
        if(inputTokens.Length > 0)
        {
            // sanitize the inputs, such that tokens are only taken 
            // into account once not empty
            // this allows the format prediction to show up even after pressing space
            // (before starting to type the argument(s))
            List<string> nonEmptyTokens = new List<string>();
            foreach (var tok in inputTokens)
            {
                if(tok != "")
                    nonEmptyTokens.Add(tok);
            }
            inputTokens = nonEmptyTokens.ToArray();
        }

        for (int i = 0; i < commands.Count; i++)
        {
            string commandName = commands[i].name;
            string commandFormat = commands[i].format;

            // if the input is a complete command name
            if(inputTokens[0] == commandName)
            {
                // create a prediction string
                string predi = "";

                // loop through the tokens in the command's format string
                string[] formatTokens = commandFormat.Split(' ');
                for (int e = 0; e < formatTokens.Length; e++)
                {
                    // for each token but the first, add a space
                    if(e != 0)
                        predi += " ";

                    // for every token present in the input (e < inputTokens.Length),
                    // leave it as it is in the input in the prediction
                    // but for every other tokens in the format, add it to the prediction
                    if(e < inputTokens.Length)
                        predi += inputTokens[e];
                    else
                        predi += formatTokens[e];
                }
                predictions.Add(predi);
                break;
            }
            // otherwise, if the command name contains it somewhere
            else if(commandName.Contains(inputTokens[0]))
            {
                // if the command name starts with the input
                // then put that prediction first
                // Otherwise, just add it at the end

                // TODO: add better ordering of predictions depending on relevance
                if(commandName.StartsWith(inputTokens[0]))
                    predictions.Insert(0, commandName);
                else
                    predictions.Add(commandName);
            }
        }
    }
}

// This class is used for a representation and a way of transferring
// messages to other classes from the console controller
// A custom graphical interface for the console can (and should if needed)
// implement its own message class for storing and formatting messages
// (eg. The GUIMessage used by the ConsoleGUI class)
public class ConsoleMessage
{
    public enum MessageTypes { Log, Debug, Error }
    public enum ErrorTypes { MissingArgument, InvalidArgument, InvalidCommand }

    public readonly MessageTypes type;
    
    public readonly string source;
    public readonly string content;

    private ConsoleMessage(string content, string source, MessageTypes type = MessageTypes.Log)
    {
        this.content = content;
        this.source = source;
        this.type = type;
    }

    public static ConsoleMessage Log(string content, string source = "")
    {
        return new ConsoleMessage(content, source, MessageTypes.Log);
    }
    public static ConsoleMessage Debug(string content, string source = "")
    {
        return new ConsoleMessage(content, source, MessageTypes.Debug);
    }
    public static ConsoleMessage Error(ErrorTypes error, string input, string expected = "UNSPECIFIED", string source = "")
    {
        string content;
        switch (error)
        {
            case ErrorTypes.MissingArgument:
                content = $"\'{input}\' : Missing argument. Expected: \'{expected}\'";
                break;
            case ErrorTypes.InvalidArgument:
                content = $"\'{input}\' : Invalid argument. Expected: \'{expected}\'";
                break;
            case ErrorTypes.InvalidCommand:
                content = $"\'{input}\' : Invalid command.";
                break;
            default:
                content = $"\'{input}\' : Unknown Error";
                break;
        }

        return new ConsoleMessage(content, source, MessageTypes.Error);
    }
}