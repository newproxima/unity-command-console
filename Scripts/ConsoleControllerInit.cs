using System;
using System.Collections.Generic;

public partial class ConsoleController
{
    public static ActionCommand<string> HELP;
    public static ActionCommand CLIST;
    public static ActionCommand CLEAR;

    // DEFINE NEW COMMAND'S STATIC FIELDS HERE

    public void CommandsInit()
    { 
				// Here are initialized the different commands by assigning
				// an instance of a daughter class of CommandBase (eg. ActionCommand)
				// to a predefined static field
        HELP = new ActionCommand<string>("help", "Provides information about a command", "help [command_name]",
        (c) => {
            int index = FindCommandIndex(c);
            if(index >= 0 && index < commands.Count)
            {
                var com = commands[index];
                PrintToConsole(ConsoleMessage.Log($"\'{com.format}\' | {com.description}", "HELP"));
            }
            else
            {
                PrintToConsole(ConsoleMessage.Error(ConsoleMessage.ErrorTypes.InvalidCommand, c, "", "HELP"));
            }
        });

        CLIST = new ActionCommand("clist", "Prints a list of all the available commands", "clist",
        () => {
            PrintToConsole(ConsoleMessage.Log("Available commands:", "CLIST"));
            foreach (var com in commands)
            {
                PrintToConsole(ConsoleMessage.Log($"   {com.name} \'{com.format}\' : {com.description}"));
            }
        });

        CLEAR = new ActionCommand("clear", "Clears the console", "clear", 
        () => {
            ConsoleCleared?.Invoke(this, EventArgs.Empty);
        });

        // INITIALIZE STATIC FIELDS HERE


        // Only commands added here will be shown and usable in the console.
        commands = new List<BaseCommand>
        {
            HELP,
            CLIST,
            CLEAR,
            // ADD NEW COMMANDS HERE
        };
    }
}
