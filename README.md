# Unity Command Console

C# source code files to create a command console within the Unity Engine.

### Usage

The 4 .cs files in the 'Scripts' folder may be added to any Unity project and will work out of the box. 

To create a console in a scene, add both  `ConsoleController` & `ConsoleGUI` script instances to any game object, and the console will be up and running ! To open it you may press the default '$' (`KeyCode.DOLLAR`) key or map any key to a Unity Input button named "ToggleConsole".

Now, to add a new command to be usable within the console you must edit the [ConsoleControllerInit.cs](Scripts/ConsoleControllerInit.cs) file. Documentation is found inlined in the code.

If additional command types are required (notably to make use of more arguments), they may be defined in the [ConsoleCommand.cs](Scripts/ConsoleCommand.cs) file.